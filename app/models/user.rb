load 'tools/shopify_tools.rb'

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :artworks

  validates :display_name, presence: true
  validates :display_name, uniqueness: true
  validates :display_name, format: { with: /\A[a-zA-Z\d\s]+\z/,
    message: "only allows letters, digits and spaces" }, allow_blank: true

  validates :name, presence: true

  after_create do
    create_redirect(self)
  end
end
